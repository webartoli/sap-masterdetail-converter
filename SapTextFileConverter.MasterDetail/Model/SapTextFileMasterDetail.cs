﻿using System.Collections.Generic;

namespace SapTextFileConverter.MasterDetail.Model
{
    class SapTextFileMasterDetail
    {
        public string Title { get; set; }
        public string[] Header { get; set; }

        public Column[] ColumnHeadMaster { get; set; }
        public Column[] ColumnHeadDetail { get; set; }

        public List<Row> Data { get; set; }

        public SapTextFileMasterDetail()
        {
            Data = new List<Row>();
        }
    }
}