using System.Diagnostics;
using System.Linq;

namespace SapTextFileConverter.MasterDetail.Model
{
    [DebuggerDisplay("Col: {IsEmpty ? \"Empty\" : Title}")]
    class Column
    {
        public string Title { get; set; }
        public bool IsEmpty => string.IsNullOrWhiteSpace(Title);

        public int Index { get; }

        private Column(string title, int index)
        {
            Title = title.Trim();
            Index = index;
        }

        public static Column[] GetColumns(string buffer)
        {
            return buffer.Split(SapTextConfig.Separator).Select((x,i) => new Column(x,i)).ToArray();
        }
    }
}