﻿using System.Collections.Generic;

namespace SapTextFileConverter.MasterDetail.Model
{
    class Row
    {
        public string[] Master { get; set; }
        public List<string[]> Detail { get; }

        public Row()
        {
            Detail = new List<string[]>();
        }
    }
}