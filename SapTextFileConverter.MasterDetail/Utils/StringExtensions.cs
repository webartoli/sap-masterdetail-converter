﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SapTextFileConverter.MasterDetail.Utils
{
    static class StringExtensions
    {
        public static string[] SplitAndTrimBy(this string buffer, char separator)
        {
            return buffer.Split(separator).Select(x => x.Trim()).ToArray();
        }

        public static string SafeJoinWithSeparator(this IEnumerable<string> stringCollection)
        {
            return stringCollection.Select(Wrap).Join(SapTextConfig.Separator);
        }

        private static string Join(this IEnumerable<string> stringCollection, char separator)
        {
            return String.Join(separator.ToString(), stringCollection);
        }

        private static string Wrap(string str)
        {
            const char separator = SapTextConfig.Separator;
            const string delimiter = @"""";

            if (str.Contains(separator) || String.IsNullOrWhiteSpace(str))
            {
                return $@"{delimiter}{str}{delimiter}";
            }
            return str;
        }

        public static string TryFetchData(this string[] buffer, int index)
        {
            try
            {
                return buffer[index];
            }
            catch
            {
                return String.Empty;
            }
        }
    }
}