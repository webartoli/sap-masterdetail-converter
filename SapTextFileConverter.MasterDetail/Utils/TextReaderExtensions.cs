﻿using System.IO;

namespace SapTextFileConverter.MasterDetail.Utils
{
    static class TextReaderExtensions
    {
        public static string GetNextNonEmptyLine(this TextReader reader)
        {
            string line;

            while (!GetNextLine(reader, out line))
            {
            }

            return line;
        }

        public static bool GetNextLine(this TextReader reader, out string line)
        {
            line = reader.GetTrimmedLine();
            return !string.IsNullOrWhiteSpace(line);
        }

        public static string GetTrimmedLine(this TextReader reader)
        {
            return reader.ReadLine()?.Trim() ?? string.Empty;
        }
    }
}