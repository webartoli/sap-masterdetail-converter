﻿using System.IO;
using System.Text;

namespace SapTextFileConverter.MasterDetail.Utils
{
    public static class FileInfoExtensions
    {
        public static FileInfo AppendToName(this FileInfo fileInfo, string str)
        {
            var fileName = fileInfo.NameWithoutExtension() + str + fileInfo.Extension;

            return new FileInfo(Path.Combine(fileInfo.Directory.FullName, fileName));
        }

        private static string NameWithoutExtension(this FileInfo fileInfo)
        {
            return Path.GetFileNameWithoutExtension(fileInfo.Name);
        }


        /// <summary>
        /// Determines a text file's encoding by analyzing its byte order mark (BOM).
        /// Defaults to ASCII when detection of the text file's endianness fails.
        /// </summary>
        /// <param name="fileInfo">The text file to analyze.</param>
        /// <returns>The detected encoding.</returns>
        public static Encoding GetEncoding(this FileInfo fileInfo)
        {
            // Read the BOM
            var bom = new byte[4];
            using (var file = new FileStream(fileInfo.FullName, FileMode.Open, FileAccess.Read))
            {
                file.Read(bom, 0, 4);
            }

            // Analyze the BOM
            if (bom[0] == 0x2b && bom[1] == 0x2f && bom[2] == 0x76) return Encoding.UTF7;
            if (bom[0] == 0xef && bom[1] == 0xbb && bom[2] == 0xbf) return Encoding.UTF8;
            if (bom[0] == 0xff && bom[1] == 0xfe) return Encoding.Unicode; //UTF-16LE
            if (bom[0] == 0xfe && bom[1] == 0xff) return Encoding.BigEndianUnicode; //UTF-16BE
            if (bom[0] == 0 && bom[1] == 0 && bom[2] == 0xfe && bom[3] == 0xff) return Encoding.UTF32;
            return Encoding.ASCII;
        }
    }
}