﻿using System.IO;
using System.Linq;
using System.Text;
using SapTextFileConverter.MasterDetail.Model;
using SapTextFileConverter.MasterDetail.Utils;

namespace SapTextFileConverter.MasterDetail
{
    class SapTextFileMasterDetailWriter
    {
        private readonly Encoding _encoding;

        public SapTextFileMasterDetailWriter(Encoding encoding)
        {
            _encoding = encoding;
        }

        public void Write(SapTextFileMasterDetail document, FileInfo fileInfo)
        {
            using (var writer = new StreamWriter(new FileStream(fileInfo.FullName, FileMode.Open, FileAccess.ReadWrite), _encoding))
            {
                var masterHeader = WriteHeader(document, writer, out var detailHeader); 

                foreach (var row in document.Data)
                {
                    WriteData(row, masterHeader, detailHeader, writer);
                }
            }
        }

        private static Column[] WriteHeader(SapTextFileMasterDetail document, StreamWriter writer, out Column[] detailHeader)
        {
            var masterHeader = document.ColumnHeadMaster
                .Where(x => !x.IsEmpty)
                .ToArray();

            detailHeader = document.ColumnHeadDetail
                .Where(x => !x.IsEmpty)
                .ToArray();

            var lineHeader = masterHeader.Union(detailHeader).Select(x => x.Title).SafeJoinWithSeparator();
            writer.WriteLine(lineHeader);
            return masterHeader;
        }

        private void WriteData(Row row, Column[] masterHeader, Column[] detailHeader, TextWriter writer)
        {
            var masterPart = masterHeader.Select(column => row.Master.TryFetchData(column.Index)).ToArray();

            if (row.Detail.Any())
            {
                foreach (var detail in row.Detail)
                {
                    var rowPart = masterPart.ToList();
                    rowPart.AddRange(detailHeader.Select(column => detail.TryFetchData(column.Index)));

                    writer.WriteLine(rowPart.SafeJoinWithSeparator());
                }
            }
            else
            {
                writer.WriteLine(masterPart.SafeJoinWithSeparator());
            }
        }
    }
}