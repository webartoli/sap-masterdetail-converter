﻿using System.IO;
using System.Text;
using SapTextFileConverter.MasterDetail.Model;
using SapTextFileConverter.MasterDetail.Utils;

namespace SapTextFileConverter.MasterDetail
{
    class SapTextFileMasterDetailReader
    {
        private readonly Encoding _enconding;

        public SapTextFileMasterDetailReader(Encoding enconding)
        {
            _enconding = enconding;
        }

        public SapTextFileMasterDetail Read(FileInfo fileInfo)
        {
            var result = new SapTextFileMasterDetail();

            using (var reader = new StreamReader(fileInfo.FullName, _enconding))
            {
                Headers(result, reader);

                Columns(result, reader);

                Data(reader, result);
            }

            return result;
        }

        private static void Data(StreamReader reader, SapTextFileMasterDetail result)
        {
            while (!reader.EndOfStream)
            {
                var row = new Row
                {
                    Master = reader.GetNextNonEmptyLine().SplitAndTrimBy(SapTextConfig.Separator)
                };

                while (reader.GetNextLine(out var line))
                {
                    row.Detail.Add(line.SplitAndTrimBy(SapTextConfig.Separator));
                }

                result.Data.Add(row);
            }
        }

        private static void Columns(SapTextFileMasterDetail result, StreamReader reader)
        {
            result.ColumnHeadMaster = Column.GetColumns(reader.GetTrimmedLine());
            result.ColumnHeadDetail = Column.GetColumns(reader.GetTrimmedLine());
        }

        private static void Headers(SapTextFileMasterDetail result, StreamReader reader)
        {
            result.Title = reader.GetTrimmedLine();
            result.Header = new[] { reader.GetTrimmedLine(), reader.GetTrimmedLine() };
        }

    }
}