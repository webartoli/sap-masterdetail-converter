﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Sap MasterDetail Converter")]
[assembly: AssemblyDescription("Converte un file SAP master-detail in un plain text per le sole colonne popolate")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Claudio Bartoli")]
[assembly: AssemblyProduct("SapTextFileConverter.MasterDetail")]
[assembly: AssemblyCopyright("Copyright © Claudio Bartoli 2016")]
[assembly: AssemblyTrademark("Claudio Bartoli")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("01d163d4-7db4-483d-a746-b5935a7164c8")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
