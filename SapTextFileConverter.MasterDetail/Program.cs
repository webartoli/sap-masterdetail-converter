﻿using System;
using System.IO;
using SapTextFileConverter.MasterDetail.Utils;

namespace SapTextFileConverter.MasterDetail
{
    class Program
    {
        static void Main(string[] args)
        {
            foreach (var filename in args)
            {
                Flatten(filename);
            }
        }

        private static void Flatten(string filename)
        {
            var file = new FileInfo(filename);
            var enconding = file.GetEncoding();

            if (!file.Exists)
            {
                Console.WriteLine($"Unable to access file {file.Name}. Please double check its path and permissions");
                return;
            }

            var reader = new SapTextFileMasterDetailReader(enconding);
            var document = reader.Read(file);

            var flattenedFile = file.AppendToName(".flattened");

            var writer = new SapTextFileMasterDetailWriter(enconding);
            writer.Write(document, flattenedFile);

            Console.WriteLine($"Created flattened file {flattenedFile.Name}");
        }
    }
}
